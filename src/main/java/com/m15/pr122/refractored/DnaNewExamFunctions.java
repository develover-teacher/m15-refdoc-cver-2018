package com.m15.pr122.refractored;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Classe amb funcions de tractament de cadenes d'ADN.
 * @author miqamorosal
 * @date 03/12/2019
 */
public class DnaNewExamFunctions {

    /**
     * Permet buscar un tros de seqüència i saber si hi és o no.
     * @param DNA
     * @param trosDNA
     * @return Retorna cert si existeix el trosDNA a DNA
     */
    public boolean existeixTrosCadena(String DNA, String trosDNA) {
        return buscarNumVegadesTrosCadena(DNA,trosDNA)>0;
    }
    
    /**
     * Permet buscar un tros de seqüència que es repeteix 1 o més cops
     * @param DNA
     * @param trosDNA 
     * @return Numero de vegades que apareix el trosDNA a DNA
     */
    public int buscarNumVegadesTrosCadena(String DNA, String trosDNA) {
        Pattern p = Pattern.compile(trosDNA);
        Matcher matcher = p.matcher(DNA);
        int numVegades = 0;
        while(matcher.find()) {
            numVegades++;
        }
        return numVegades;
    }
    
    /**
     * Permet esborrar un tros de seqüència que es repeteix 1 o més cops.
     * @param DNA
     * @param trosDNA
     * @return Cadena sense cap tros de sequencia 
     */
    public String esborrarTrosCadena(String DNA, String trosDNA) {
       String resultDNA = "";
       if (DNA.contains(trosDNA)) { 
            resultDNA = DNA.replaceAll(trosDNA, ""); 
        } 
        System.out.println("resultDNA="+resultDNA);
        return resultDNA;
    }
    
   
}
