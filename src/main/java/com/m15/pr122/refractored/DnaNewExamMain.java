package com.m15.pr122.refractored;

import com.m15.filereaders.FileReaders;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author tarda
 */
public class DnaNewExamMain {

    /**
     * @param args the command line arguments
     * @throws java.io.IOException
     */
    public static void main(String[] args) throws IOException {
        DnaNewExamMain myApp = new DnaNewExamMain();
        myApp.run();
    }

    /**
     * Function that prints the menu and returns the option chosen
     *
     * @return the selected option or -1 in case of error
     */
    private int printMenu() {
        int option = -1;

        System.out.println("\n Tria una opció:");
        System.out.println("0.-Exit");
        // OLD MENU
//        System.out.println("1.- Buscar un tros de la seqüència (bases juntes) i dir quants cops hi apareix");
//        System.out.println("2.- Esborrar un tros de la seqüència (bases juntes), i ens retornarà el tros original d’ADN però sense el tros escollit.");
        
        // NEW MENU
        System.out.println("1.- Es pot buscar un tros de seqüència i saber si hi és o no.");
        System.out.println("2.- Es pot buscar un tros de seqüència que es repeteix 1 o més cops");
        System.out.println("3.- Es pot esborrar un tros de seqüència que es repeteix 1 o més cops.");
        
        System.out.print("\nOpció: ");

        try {
            Scanner myScan = new Scanner(System.in);
            option = myScan.nextInt();
            myScan.nextLine();
        } catch (Exception e) {
            System.out.println((char)27 + "[31m" + "Data error" + (char)27 + "[0m");
        }

        return option;
    }

    /**
     * Function that runs the app
     */
    private void run() {
        int option = 0;
        DnaNewExamFunctions dnaFunct = new DnaNewExamFunctions();
        FileReaders file = new FileReaders();
        ArrayList<String> dnaSequence_list = file.readSequence("src/main/java/com/m15/filereaders/dnaSequence.txt");
        String dnaSequence = String.join("", dnaSequence_list);
        // DNA Subsequence.
        String dnaSubSequence = "GAT";
        dnaSequence = dnaSequence.toUpperCase();

        do {
            option = printMenu();
            switch (option) {
                case 0:
                    System.out.println("Bye!");
                    break;
                case 1:
                    ShowSecuences(dnaSequence, dnaSubSequence);
                    System.out.println("Exists ??");
                    System.out.println(dnaFunct.existeixTrosCadena(dnaSequence, dnaSubSequence));
                    break;
                case 2:
                    ShowSecuences(dnaSequence, dnaSubSequence);
                    System.out.println("Number of times that appears");
                    System.out.println(dnaFunct.buscarNumVegadesTrosCadena(dnaSequence, dnaSubSequence));
                    break;
                case 3:
                    ShowSecuences(dnaSequence, dnaSubSequence);
                    dnaFunct.esborrarTrosCadena(dnaSequence, dnaSubSequence);
                    break;
                default:
                    System.out.println((char)27 + "[31m" + "Not a valid option" + (char)27 + "[0m");
                    break;
            }
        } while (option != 0);
    }

    private void ShowSecuences(String dnaSequence, String dnaSubSequence) {
        System.out.println("DNA sequence: " + dnaSequence);
        System.out.println("DNA subSequence: " + dnaSubSequence);
    }
}
