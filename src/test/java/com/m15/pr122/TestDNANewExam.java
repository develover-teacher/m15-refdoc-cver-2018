/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.m15.pr122;

import com.m15.pr122.refractored.DnaNewExamFunctions;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

/**
 *
 * @author mati
 */
public class TestDNANewExam {
    
    String dnaSequence = "gatactgatacatadctcata";;
    DnaNewExamFunctions dnaFunct = new DnaNewExamFunctions();
        
    public TestDNANewExam() {
    }
    
    @BeforeAll
    public static void setUpClass() {
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
        dnaSequence = "gatactgatacatadctcata";
        dnaFunct = new DnaNewExamFunctions();
    }
    
    @AfterEach
    public void tearDown() {
    }

    @Test
    public void testExistingDNA() {
        String ExistingSubSequence = "gat";
        boolean expected = true;
        boolean result = dnaFunct.existeixTrosCadena(dnaSequence, ExistingSubSequence);
        assertEquals(result,expected);
    }
    
    @Test
    public void testNotExistingDNA() {
        String NonExistingSubSequence = "gaaaaaa";
        boolean expected = false;
        boolean result = dnaFunct.existeixTrosCadena(dnaSequence, NonExistingSubSequence);
        assertEquals(result,expected);
    }
    
}
